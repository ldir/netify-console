<?php
// Netify Console
// Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

define('NC_CHAR_DASH', (NC_USE_UTF8) ?  '–' : '-');
define('NC_CHAR_GT', (NC_USE_UTF8) ? '❯' : '>');
define('NC_CHAR_DOTDOTDOT', (NC_USE_UTF8) ? '…' : '...');

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
