<?php
// Netify Console
// Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class ncFlow
{
    public static $ethers = null;
    public static $rdns_lookup = false;
    public static $display_hash = false;

    protected static $dns_cache = array();
    protected static $max_ifname = 0;
    protected static $max_ip_proto = 0;
    protected static $max_nd_proto = 0;
    protected static $max_app = 0;
    protected static $max_src = 0;
    protected static $max_dst = 0;

    static function ndots_trim($host, $ndots = 2) {
        $parts = explode('.', $host);
        if (count($parts) > $ndots) {
            for ($i = 0; $i < $ndots; $i++)
                array_pop($parts);
            return implode('.', $parts);
        }

        return $host;
    }

    static function render($flow, &$col_adjust)
    {
        $proto = str_replace('ipv6-', '', getprotobynumber($flow->flow->ip_protocol));
        $ip_proto = strtoupper($proto . (($flow->flow->ip_version == 4) ? '4' : '6'));

        $detected_proto = str_replace('V6', '', $flow->flow->detected_protocol_name);

        $app_id = (property_exists($flow->flow, 'detected_service')) ?
            $flow->flow->detected_service : $flow->flow->detected_application;
        $app_name = (property_exists($flow->flow, 'detected_service_name')) ?
            $flow->flow->detected_service_name : $flow->flow->detected_application_name;

        $detected_application = '';
        if ($app_id > 0) {
            $search = array('/^\d+\./', '/^\w+\./i');
            $detected_application = preg_replace($search, '', $app_name);
        }

        $local_addr = $flow->flow->local_ip;
        $other_addr = $flow->flow->other_ip;

        if (self::$rdns_lookup) {
            if (array_key_exists($flow->flow->local_ip, self::$dns_cache))
                $local_addr = self::$dns_cache[$flow->flow->local_ip];
            else if (($host = gethostbyaddr($flow->flow->local_ip)) !== false
                && $host != $flow->flow->local_ip) {
                if (($os = strpos($host, ".")) !== false) {
                    $host = substr($host, 0, $os);
                    $local_addr = self::ndots_trim($host);
                }

                self::$dns_cache[$flow->flow->local_ip] = $local_addr;
            }

            if (array_key_exists($flow->flow->other_ip, self::$dns_cache))
                $other_addr = self::$dns_cache[$flow->flow->other_ip];
            else if (($host = gethostbyaddr($flow->flow->other_ip)) !== false
                && $host != $flow->flow->other_ip) {

                $os = explode('.', $host);

                if (count($os) > 3) {
                    $other_addr = '';
                    for ($i = count($os) - 3; $i < count($os); $i++)
                        $other_addr .= $os[$i] . '.';
                    $other_addr = rtrim($other_addr, '.');
                }
                else
                    $other_addr = $host;

                self::$dns_cache[$flow->flow->other_ip] = $other_addr;
            }
        }

        if (is_array(self::$ethers) && count(self::$ethers)) {
            if ($local_addr == $flow->flow->local_ip &&
                array_key_exists($flow->flow->local_mac, self::$ethers))
                $local_addr = self::ndots_trim(self::$ethers[$flow->flow->local_mac]);

            if ($flow->flow->other_type == 'other' &&
                $other_addr == $flow->flow->other_ip &&
                array_key_exists($flow->flow->other_mac, self::$ethers))
                $other_addr = self::ndots_trim(self::$ethers[$flow->flow->other_mac]);
        }

        if ($flow->flow->local_origin) {
            $src = sprintf("%s%s", $local_addr,
                ($flow->flow->local_port > 0) ? ':' . $flow->flow->local_port : ''
            );
            $dst = sprintf("%s%s", $other_addr,
                ($flow->flow->other_port > 0) ? ':' . $flow->flow->other_port : ''
            );
        }
        else {
            $dst = sprintf("%s%s", $local_addr,
                ($flow->flow->local_port > 0) ? ':' . $flow->flow->local_port : ''
            );
            $src = sprintf("%s%s", $other_addr,
                ($flow->flow->other_port > 0) ? ':' . $flow->flow->other_port : ''
            );
        }

        if (strlen($flow->interface) > self::$max_ifname)
            self::$max_ifname = strlen($flow->interface);
        if (strlen($ip_proto) > self::$max_ip_proto)
            self::$max_ip_proto = strlen($ip_proto);
        if (strlen($detected_proto) > self::$max_nd_proto)
            self::$max_nd_proto = strlen($detected_proto);
        if (strlen($detected_application) > self::$max_app)
            self::$max_app = strlen($detected_application);
        if (strlen($src) > self::$max_src)
            self::$max_src = strlen($src);
        if (strlen($dst) > self::$max_dst)
            self::$max_dst = strlen($dst);

        $col_adjust = self::$max_ifname + self::$max_ip_proto +
            self::$max_nd_proto + self::$max_app + self::$max_src + self::$max_dst;

        $format = sprintf(
            ' %%s%%s%%s%%s%%s%%s %%-%ds%%s%%-%ds %%-%ds %%-%ds %%-%ds %s %%-%ds',
            self::$max_ifname,
            self::$max_ip_proto, self::$max_nd_proto,
            self::$max_app, self::$max_src, NC_CHAR_GT, self::$max_dst
        );

        $text = array();

        $text[] = sprintf(
            $format,
            ($flow->internal) ? 'i' : 'e',
            ($flow->flow->ip_nat) ? 'n' : NC_CHAR_DASH,
            (property_exists($flow->flow, 'detection_updated') && $flow->flow->detection_updated) ? 'u' : NC_CHAR_DASH,
            ($flow->flow->detection_guessed) ? 'g' : NC_CHAR_DASH,
            ($flow->flow->dhc_hit) ? 'd' : NC_CHAR_DASH,
            (property_exists($flow->flow, 'soft_dissector') && $flow->flow->soft_dissector) ? 's' : NC_CHAR_DASH,
            $flow->interface,
            (self::$display_hash) ? ' ' . substr($flow->flow->digest, 0, 7) . ' ' : ' ',
            $ip_proto,
            $detected_proto,
            $detected_application,
            $src, $dst
        );

        $flow_meta = array();

        if (property_exists($flow->flow, 'host_server_name'))
            $flow_meta[] = sprintf('H: %s', $flow->flow->host_server_name);
        if (property_exists($flow->flow, 'http') &&
            property_exists($flow->flow->http, 'user_agent'))
            $flow_meta[] = sprintf('UA: %s', $flow->flow->http->user_agent);
        if (property_exists($flow->flow, 'ssh') &&
            property_exists($flow->flow->ssh, 'client'))
            $flow_meta[] = sprintf('CA: %s', $flow->flow->ssh->client);
        if (property_exists($flow->flow, 'ssh') &&
            property_exists($flow->flow->ssh, 'server'))
            $flow_meta[] = sprintf('SA: %s', $flow->flow->ssh->server);
        if (property_exists($flow->flow, 'ssl')) {
            $flow_meta[] = sprintf('SV: %s', $flow->flow->ssl->version);
            $flow_meta[] = sprintf('CS: %s', $flow->flow->ssl->cipher_suite);
            if (property_exists($flow->flow->ssl, 'client'))
                $flow_meta[] = sprintf('C: %s', $flow->flow->ssl->client);
            else if (property_exists($flow->flow->ssl, 'client_sni'))
                $flow_meta[] = sprintf('C: %s', $flow->flow->ssl->client_sni);
            if (property_exists($flow->flow->ssl, 'server'))
                $flow_meta[] = sprintf('S: %s', $flow->flow->ssl->server);
            else if (property_exists($flow->flow->ssl, 'server_cn'))
                $flow_meta[] = sprintf('S: %s', $flow->flow->ssl->server_cn);
        }
        if (property_exists($flow->flow, 'bt'))
            $flow_meta[] = sprintf('IH: %s', $flow->flow->bt->info_hash);
        if (property_exists($flow->flow, 'mdns'))
            $flow_meta[] = sprintf('A: %s', $flow->flow->mdns->answer);

        if (count($flow_meta)) {
            $text[0] .= ' ' . implode(' ', $flow_meta);
        }

        return $text;
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
