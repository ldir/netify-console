<?php
// Netify Console
// Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

class ncRect
{
    public $x, $y, $w, $h;

    function __construct($x = 0, $y = 0, $w = 0, $h = 0)
    {
        $this->x = $x;
        $this->y = $y;
        $this->w = $w;
        $this->h = $h;
    }
}

class ncScreen
{
    public $rect;

    function __construct()
    {
        ncurses_init();

        ncurses_curs_set(0);
        ncurses_flushinp();
        ncurses_raw();
        ncurses_noecho();
        ncurses_timeout(0);

        if (ncurses_has_colors()) {
            ncurses_start_color();
            ncurses_use_default_colors();
        }

        $this->rect = new ncRect();
        $this->resize();
    }

    function __destruct()
    {
        ncurses_end();
    }

    public function refresh()
    {
        ncurses_refresh();
    }

    public function refresh_size()
    {
        ncurses_getmaxyx(STDSCR, $this->rect->h, $this->rect->w);
    }

    public function width()
    {
        $this->refresh_size();
        return $this->rect->w;
    }

    public function height()
    {
        $this->refresh_size();
        return $this->rect->h;
    }

    public function resize()
    {
        ncurses_erase();
        $this->refresh_size();
    }

    public function was_resized()
    {
        $w = 0; $h = 0;
        ncurses_getmaxyx(STDSCR, $h, $w);

        if ($w != $this->rect->w || $h != $this->rect->h) return true;

        return false;
    }

    public function too_small()
    {
        $w = 0; $h = 0;
        ncurses_getmaxyx(STDSCR, $h, $w);

        if ($w < NC_SCREEN_MIN_WIDTH || $h < NC_SCREEN_MIN_HEIGHT) return true;

        return false;
    }

    public function read_key()
    {
        $fds_read = array(STDIN);
        $fds_write = null;
        $fds_except = array(STDIN);

        $rc = stream_select($fds_read, $fds_write, $fds_except, 0);

        if ($rc < 0) throw new Exception('Select on STDIN failed.');

        if ($rc > 0 &&
            in_array(STDIN, $fds_read) || in_array(STDIN, $fds_except))
            return ncurses_getch();

        return 0;
    }

    public function flush_input()
    {
        ncurses_flushinp();
    }

    public function run_command($command, $args)
    {
        ncurses_end();

        switch (($pid = pcntl_fork())) {
        case 0:
            pcntl_exec($command, $args);
        case -1:
            break;
        default:
            pcntl_waitpid($pid, $status);
        }

        ncurses_refresh();
    }
}

class ncWindow
{
    public $sh;
    public $wh;
    public $rect;

    function __construct($sh, $x = 0, $y = 0, $w = 0, $h = 0)
    {
        $this->sh = $sh;
        $this->rect = new ncRect($x, $y, $w, $h);
        $this->wh = ncurses_newwin(
            $this->rect->h, $this->rect->w,
            $this->rect->y, $this->rect->x
        );

        if (! is_resource($this->wh)) {
            throw new Exception(
                sprintf('Failed to create window: %dx%d %dx%d',
                    $this->rect->x, $this->rect->y,
                    $this->rect->w, $this->rect->h
                )
            );
        }

        ncurses_werase($this->wh);
    }

    function __destruct()
    {
        if (is_resource($this->wh))
            ncurses_delwin($this->wh);
    }

    public function resize($x = 0, $y = 0, $w = 0, $h = 0)
    {
        $this->rect->x = $x;
        $this->rect->y = $y;
        $this->rect->w = $w;
        $this->rect->h = $h;

        if (! is_resource($this->wh))
            throw new Exception('Window is invalid.');

        ncurses_delwin($this->wh);

        $this->wh = ncurses_newwin(
            $this->rect->h, $this->rect->w,
            $this->rect->y, $this->rect->x
        );

        if (! is_resource($this->wh)) {
            throw new Exception(
                sprintf('Failed to resize window: %dx%d %dx%d',
                    $this->rect->x, $this->rect->y,
                    $this->rect->w, $this->rect->h
                )
            );
        }
    }

    public function refresh()
    {
        ncurses_wrefresh($this->wh);
    }
}

class ncWindowTitle extends ncWindow
{
    const TITLE_LEFT = 1;
    const TITLE_RIGHT = 2;

    protected $title_left;
    protected $title_right;

    function __construct($sh, $x = 0, $y = 0, $w = 0, $h = 0)
    {
        parent::__construct($sh, $x, $y, $w, $h);
    }

    public function resize($x = 0, $y = 0, $w = 0, $h = 0)
    {
        parent::resize($x, $y, $w, $h);

        $this->set_title(self::TITLE_LEFT, $this->title_left);
        $this->set_title(self::TITLE_RIGHT, $this->title_right);
    }

    public function set_title($which, $title)
    {
        switch ($which) {
        case self::TITLE_LEFT:
            $this->title_left = trim($title);
            break;

        case self::TITLE_RIGHT:
            $this->title_right = trim($title);
            break;
        }

        ncurses_wattron($this->wh, NCURSES_A_REVERSE);

        for ($x = 0; $x < $this->rect->w; $x++)
            ncurses_mvwaddstr($this->wh, 0, $x, ' ');

        $title_left = substr($this->title_left, 0, $this->rect->w);
        $title_right = substr($this->title_right, 0, $this->rect->w);

        ncurses_wattron($this->wh, NCURSES_A_BOLD);
        ncurses_mvwaddstr($this->wh, 0, 0, $title_left);

        ncurses_wattroff($this->wh, NCURSES_A_BOLD);
        if (strlen($title_left) + 1 + strlen($title_right) <= $this->rect->w)
            ncurses_mvwaddstr($this->wh, 0, $this->rect->w - strlen($title_right), $title_right);

        ncurses_wattroff($this->wh, NCURSES_A_REVERSE);
    }
}

class ncWindowStatus extends ncWindowTitle
{
    protected $title_left_prev = '';

    function __construct($sh, $x = 0, $y = 0, $w = 0, $h = 0)
    {
        parent::__construct($sh, $x, $y, $w, $h);
    }

    public function set_paused($paused)
    {
        if ($paused) {
            $this->title_left_prev = $this->title_left;
            $this->set_title(self::TITLE_LEFT, '[PAUSED]');
        }
        else
            $this->set_title(self::TITLE_LEFT, $this->title_left_prev);
    }

    public function set_status($status)
    {
        $flows = 0;
        $flows_prev = 0;

        if (property_exists($status, 'flows')) $flows = intval($status->flows);
        else if (property_exists($status, 'flow_count')) $flows = intval($status->flow_count);
        if (property_exists($status, 'flows_prev')) $flows_prev = intval($status->flows_prev);
        else if (property_exists($status, 'flow_count_prev')) $flows_prev = intval($status->flow_count_prev);

        $flows_delta = $flows - $flows_prev;
        $maxrss_delta = intval($status->maxrss_kb) - intval($status->maxrss_kb_prev);

        if ($flows_delta == $flows)
            $flows_delta = 0;
        if ($maxrss_delta == intval($status->maxrss_kb))
            $maxrss_delta = 0;

        $text_left = sprintf('Flows: %6s%s%-6s',
            number_format($flows),
            ($flows_delta > 0) ? '+' : (($flows_delta < 0) ? '-' : ''),
            ($flows_delta != 0) ? number_format(abs($flows_delta)) : ''
        );

        if (property_exists($status, 'tcm_kb')) {
            $tcm_delta = intval($status->tcm_kb) - intval($status->tcm_kb_prev);

            if ($tcm_delta == intval($status->tcm_kb))
                $tcm_delta = 0;

            $text_left .= sprintf(' Mem Active: %9s%s%-9s',
                number_format($status->tcm_kb),
                ($tcm_delta > 0) ? '+' : (($tcm_delta < 0) ? '-' : ''),
                ($tcm_delta != 0) ? number_format(abs($tcm_delta)) : ''
            );
        }

        $text_left .= sprintf(' Mem Max: %9s%s%-9s',
            number_format($status->maxrss_kb),
            ($maxrss_delta > 0) ? '+' : (($maxrss_delta < 0) ? '-' : ''),
            ($maxrss_delta != 0) ? number_format(abs($maxrss_delta)) : ''
        );

        if (property_exists($status, 'cpu_cores')) {
            $cpu_user_delta =
                $status->cpu_user - $status->cpu_user_prev;
            $cpu_system_delta =
                $status->cpu_system - $status->cpu_system_prev;

            $cpu_max_time =
                $status->update_interval * $status->cpu_cores;

            $cpu_user_percent = $cpu_user_delta * 100.0 / $cpu_max_time;
            $cpu_system_percent = $cpu_system_delta * 100.0 / $cpu_max_time;
            $cpu_total = $cpu_user_percent + $cpu_system_percent;

            $text_left .= sprintf(' CPU: %.1f%%', $cpu_total);
        }

        if ($status->uptime > 3600 * 24) {
            $days = floor($status->uptime / (3600 * 24));
            $status->uptime -= $days * 3600 * 24;
        }
        else
            $days = 0;

        if ($status->uptime > 3600) {
            $hours = floor($status->uptime / 3600);
            $status->uptime -= $hours * 3600;
        }
        else
            $hours = 0;

        if ($status->uptime > 60) {
            $minutes = floor($status->uptime / 60);
            $status->uptime -= $minutes * 60;
        }
        else
            $minutes = 0;

        $seconds = ($status->uptime > 0) ? $status->uptime : 0;

        $text_right = sprintf('%3dd %02d:%02d:%02d',
            $days, $hours, $minutes, $seconds);

        $this->set_title(self::TITLE_LEFT, $text_left);
        $this->set_title(self::TITLE_RIGHT, $text_right);
    }
}

class ncWindowScroll extends ncWindow
{
    protected $buffer;

    public $highlight = -1;

    function __construct($sh, $x = 0, $y = 0, $w = 0, $h = 0)
    {
        $this->buffer = array();

        parent::__construct($sh, $x, $y, $w, $h);
    }

    public function resize($x = 0, $y = 0, $w = 0, $h = 0)
    {
        parent::resize($x, $y, $w, $h);

        while (count($this->buffer) > $this->rect->h)
            array_shift($this->buffer);
    }

    function redraw()
    {
        ncurses_werase($this->wh);

        for ($y = 0; $y < count($this->buffer); $y++) {
            if ($y == $this->highlight) {
                ncurses_wattron($this->wh, NCURSES_A_REVERSE);
                for ($x = 0; $x < $this->rect->w; $x++)
                    ncurses_mvwaddstr($this->wh, $y, $x, ' ');
            }
            ncurses_mvwaddstr($this->wh, $y, 0, $this->buffer[$y]);
            if ($y == $this->highlight)
                ncurses_wattroff($this->wh, NCURSES_A_REVERSE);
        }
    }

    function push_line($text)
    {
        array_push($this->buffer, rtrim($text));

        if (count($this->buffer) > $this->rect->h) {
            if ($this->highlight > -1)
                $this->highlight--;
            array_shift($this->buffer);
        }

        if ($this->sh->too_small()) return;

        $this->redraw();
    }
}

class ncWindowOutput extends ncWindowScroll
{
    const MAX_OBJECTS = 100;

    protected static $col_adjust = 0;

    protected $objects;

    function __construct($sh, $x = 0, $y = 0, $w = 0, $h = 0)
    {
        $this->objects = array();

        parent::__construct($sh, $x, $y, $w, $h);
    }

    public function resize($x = 0, $y = 0, $w = 0, $h = 0)
    {
        parent::resize($x, $y, $w, $h);

        $this->buffer = array();

        foreach ($this->objects as $obj) {
            $lines = $this->render_object($obj, $redraw);

            foreach ($lines as $line)
                $this->push_line($line);
        }
    }

    public function screen_too_small()
    {
        ncurses_werase($this->wh);

        $messages = array(
            'Terminal too small, please resize',
            sprintf('to at least %dx%d.',
                NC_SCREEN_MIN_WIDTH, NC_SCREEN_MIN_HEIGHT)
        );

        if ($this->rect->h < count($messages)) return;
        $offset_y = ($this->rect->h - count($messages)) / 2;

        for ($i = 0; $i < count($messages); $i++) {
            if (strlen($messages[$i]) >= $this->rect->w)
                ncurses_mvwaddstr($this->wh, $offset_y + $i, 0, $messages[$i]);
            else {
                $offset_x = ($this->rect->w - strlen($messages[$i])) / 2;
                ncurses_mvwaddstr($this->wh,
                    $offset_y + $i, $offset_x, $messages[$i]);
            }
        }
    }

    public function push_object($obj)
    {
        array_push($this->objects, $obj);

        if (count($this->objects) == self::MAX_OBJECTS)
            array_shift($this->objects);

        $lines = $this->render_object($obj, $redraw);

        if ($redraw) {

            $this->buffer = array();

            foreach ($this->objects as $obj) {
                $lines = $this->render_object($obj, $redraw);

                foreach ($lines as $line)
                    $this->push_line($line);
            }

            return;
        }

        foreach ($lines as $line)
            $this->push_line(rtrim($line));
    }

    public function render_object($obj, &$redraw)
    {
        $redraw = false;

        if (is_array($obj))
            $objs = $obj;
        else {
            if (is_null($obj)) self::$col_adjust = 0;
            $objs = array($obj);
        }

        foreach ($objs as $obj) {
            if (is_string($obj)) {
                return array(substr($obj, 0, $this->rect->w));
            }
            else if (is_object($obj)) {
                if (property_exists($obj, 'type')) {
                    switch ($obj->type) {
                    case 'flow':
                        $lines = ncFlow::render($obj, $col_adjust);
                        if ($col_adjust > self::$col_adjust) {
                            self::$col_adjust = $col_adjust;
                            $redraw = true;
                        }
                        return $lines;
                    default:
                        return array('Unknown object type.');
                    }
                }
                else
                    return array('Unknown object type.');
            }
        }
    }
}

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
