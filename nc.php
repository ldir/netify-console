#!/usr/bin/env php
<?php
// Netify Console
// Copyright (C) 2018-2021 eGloo Incorporated <http://www.egloo.ca>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

declare(ticks = 1);

define('NC_SCREEN_MIN_WIDTH', 80);
define('NC_SCREEN_MIN_HEIGHT', 20);
define('NC_FRAME_RATE_USEC', 41667 /* 24fps */);

$paused = false;
$key_event = 0;
$applications = array();
$protocols = array();
$internal_flows = true;
$external_flows = true;
$nat_flows = true;
$nat_flows_only = false;

$sd = null;
$node = '/var/run/netifyd/netifyd.sock';
$service = 0;

if (($options = getopt('n:s:u')) === false) {
    echo "Error parsing command-line options.\n";
    exit(1);
}

if (array_key_exists('n', $options))
    $node = $options['n'];
if (array_key_exists('s', $options))
    $service = $options['s'];
if (array_key_exists('u', $options))
    define('NC_USE_UTF8', false);
else
    define('NC_USE_UTF8', true);

if (getenv('NC_BASE') !== false)
    define('NC_INCLUDE', getenv('NC_BASE'));
else
    define('NC_INCLUDE', '.');

require_once(NC_INCLUDE . '/include/nc-version.php');
require_once(NC_INCLUDE . '/include/nc-chars.php');
require_once(NC_INCLUDE . '/include/nc-flow.php');
require_once(NC_INCLUDE . '/include/nc-json.php');
require_once(NC_INCLUDE . '/include/nc-ncurses.php');
require_once(NC_INCLUDE . '/include/nc-socket.php');

$screen = new ncScreen();
$win_title = null;
$win_output = null;
$win_status = null;

function create_or_resize_windows($create = true)
{
    global $screen;
    global $win_title;
    global $win_output;
    global $win_status;

    $screen->resize();
    $screen->refresh();

    $rect_title = new ncRect(
        0, 0,
        $screen->width(), 0
    );

    $rect_output = new ncRect(
        0, $win_title->rect->h + 1,
        $screen->width(), $screen->height() - 2
    );

    $rect_status = new ncRect(
        0, $screen->height() - 1,
        $screen->width(), 0
    );

    if ($create) {
        $win_title = new ncWindowTitle($screen,
            $rect_title->x, $rect_title->y, $rect_title->w, $rect_title->h
        );
        $win_title->set_title(
            ncWindowTitle::TITLE_LEFT, 'Netify Console ©2018-2021 eGloo Incorporated');
        $win_title->set_title(
            ncWindowTitle::TITLE_RIGHT, sprintf('v%s', NC_VERSION));

        $win_output = new ncWindowOutput($screen,
            $rect_output->x, $rect_output->y, $rect_output->w, $rect_output->h
        );

        $win_status = new ncWindowStatus($screen,
            $rect_status->x, $rect_status->y, $rect_status->w, $rect_status->h
        );

        $screen->refresh();
        $win_title->refresh();
        $win_output->refresh();

        $win_status->set_title(ncWindowTitle::TITLE_LEFT, '');
        $win_status->refresh();
    }
    else {
        $win_title->resize(
            $rect_title->x, $rect_title->y, $rect_title->w, $rect_title->h
        );
        $win_title->refresh();

        $win_output->resize(
            $rect_output->x, $rect_output->y, $rect_output->w, $rect_output->h
        );
        $win_output->refresh();

        $win_status->resize(
            $rect_status->x, $rect_status->y, $rect_status->w, $rect_status->h
        );
        $win_status->refresh();

        $screen->flush_input();
    }
}

function load_ethers()
{
    global $win_output;

    $ethers = array();

    $fh = @fopen('/etc/ethers', 'r');
    if (! is_resource($fh)) return $ethers;

    do {
        $line = trim(fgets($fh));

        if (strlen($line) > 19 && substr($line, 0, 1) != '#' &&
            preg_match('/^([0-9a-f:]+)\s+([A-z0-9_:\.-]+)/',
                strtolower($line), $matches))
            $ethers[strtolower($matches[1])] = $matches[2];
    }
    while (! feof($fh));

    fclose($fh);

    $win_output->push_object(sprintf(
        "MAC address display enabled from %d ethers entries.\n", count($ethers)
    ));
    $win_output->refresh();

    return $ethers;
}

create_or_resize_windows();
sleep(2);

ncFlow::$ethers = load_ethers();

try {
    while (true) {
        $win_title->refresh();

        usleep(NC_FRAME_RATE_USEC);

        if ($screen->was_resized()) {
            create_or_resize_windows(false);
            continue;
        }
        else if ($screen->too_small()) {
            $win_output->screen_too_small();
            $win_output->refresh();
        }

        try {
            while (($key = $screen->read_key()) != 0) {

                $key_event++;

                switch ($key) {
                case 0x20:
                case 0x70:
                    if ($paused)
                        $paused = false;
                    else
                        $paused = true;

                    $win_status->set_paused($paused);
                    $win_status->refresh();
                    break;

                case 0x03:
                case 0x71:
                    $win_output->push_object('Terminating' . NC_CHAR_DOTDOTDOT);
                    $win_output->refresh();

                    sleep(1);
                    ncurses_end();

                    exit(0);

                case 0x6a: // j: Highlight down
                case 0x102: // Down
                    if ($win_output->highlight < $win_output->rect->h - 1) {
                        $win_output->highlight++;
                        $win_output->redraw();
                        $win_output->refresh();
                    }
                    break;

                case 0x6b: // k: Highlight up
                case 0x103: // Up
                    if ($win_output->highlight > -1) {
                        $win_output->highlight--;
                        $win_output->redraw();
                        $win_output->refresh();
                    }
                    break;

                case 0x61: // a: Toggle address display (MAC or IP)
                    if (ncFlow::$ethers == null)
                        ncFlow::$ethers = load_ethers();
                    else {
                        ncFlow::$ethers = null;
                        $win_output->push_object('IP address display enabled.');
                        $win_output->refresh();
                    }
                    break;

                case 0x64: // d: Local IP reverse DNS look-up on/off
                    if (ncFlow::$rdns_lookup) {
                        ncFlow::$rdns_lookup = false;
                        $win_output->push_object('Reverse DNS look-up disabled.');
                    }
                    else {
                        ncFlow::$rdns_lookup = true;
                        $win_output->push_object('Reverse DNS look-up enabled.');
                    }
                    $win_output->refresh();
                    break;

                case 0x65: // e: External flows on/off
                    if ($external_flows) {
                        $win_output->push_object('Excluding external flows.');
                        $external_flows = false;
                    }
                    else {
                        $win_output->push_object('Including external flows.');
                        $external_flows = true;
                    }
                    $win_output->refresh();
                    break;

                case 0x72:
                    ncFlow::reset_columns();
                    $win_output->render_object(null, $redraw);
                    $win_output->push_object('Reset column sizes.');
                    $win_output->refresh();
                    break;

                case 0x3f:
                case 0x68:
                case 0x109:
                    $screen->run_command('/usr/bin/man', array('netify-console'));
                    break;

                case 0x69: // i: Internal flows on/off
                    if ($internal_flows) {
                        $win_output->push_object('Excluding internal flows.');
                        $internal_flows = false;
                    }
                    else {
                        $win_output->push_object('Including internal flows.');
                        $internal_flows = true;
                    }
                    $win_output->refresh();
                    break;

                case 0x4e: // N: NAT flows only on/off
                    if ($nat_flows_only) {
                        $win_output->push_object('Including all external flows.');
                        $nat_flows_only = false;
                    }
                    else {
                        $win_output->push_object('Excluding all non-NAT external flows.');
                        $nat_flows_only = true;
                        $nat_flows = true;
                        $external_flows = true;
                    }
                    $win_output->refresh();
                    break;

                case 0x6e: // n: NAT flows on/off
                    if ($nat_flows) {
                        $win_output->push_object('Excluding NAT flows.');
                        $nat_flows = false;
                    }
                    else {
                        $win_output->push_object('Including NAT flows.');
                        $nat_flows = true;
                    }
                    $win_output->refresh();
                    break;

                case 0x48: // H: Hash column enable/disable
                    if (ncFlow::$display_hash) {
                        ncFlow::$display_hash = false;
                        $win_output->push_object('Hiding flow hash column.');
                    }
                    else {
                        ncFlow::$display_hash = true;
                        $win_output->push_object('Showing flow hash column.');
                    }
                    $win_output->refresh();
                    break;

                default:
                    $win_output->push_object(sprintf(
                        "Unhandled key event #%d: 0x%x\n", $key_event, $key
                    ));
                    $win_output->refresh();
                }
            }
        } catch (Exception $e) {
            $win_output->push_object($e->getMessage());
            $win_output->refresh();
        }

        if (! is_resource($sd)) {
            try {
                $sd = nc_socket_connect($node, $service);
                $win_output->push_object(sprintf(
                    "Connected to Netify Agent: %s%s%s\n",
                    $node, ($service != 0) ?
                        ':' : '', ($service != 0) ? $service : ''
                ));
                $win_output->refresh();
                ncurses_beep();
            } catch (Exception $e) {
                $win_output->push_object(sprintf(
                    "Error connecting to Netify Agent: %s%s%s: %s\n", $node,
                    ($service != 0) ? ':' : '', ($service != 0) ? $service : '',
                    $e->getMessage()
                ));
                $win_output->refresh();

                sleep(1);
                continue;
            }
        }

        $rc = 0;

        do {
            $fds_read = array($sd);
            $fds_write = NULL;
            $fds_except = NULL;

            try {
                if (($rc = nc_socket_select(
                    $fds_read, $fds_write, $fds_except
                )) == 0) break;
            } catch (Exception $e) {
                break;
            }

            if (in_array($sd, $fds_read)) {
                $json = null;
                try {
                    $length = nc_socket_read_payload($sd, $json);
                } catch (Exception $e) {
                    $win_output->push_object(sprintf(
                        "Exception while reading from socket: %s\n", $e->getMessage()
                    ));
                    $win_output->refresh();
                    break;
                }

                if ($length == 0) {
                    $win_output->push_object(sprintf(
                        "Netify Agent disconnected, reconnecting...\n"
                    ));
                    $win_output->refresh();
                    $sd = null;
                    sleep(1);
                    break;
                }

                switch (nc_validate_json($json)) {

                case 'flow':
                    if (! $paused) {
                        if ($json->internal && $internal_flows)
                            $win_output->push_object($json);
                        else if (! $json->internal && $external_flows) {
                            if ($json->flow->ip_nat && $nat_flows)
                                $win_output->push_object($json);
                            else if (! $json->flow->ip_nat && ! $nat_flows_only)
                                $win_output->push_object($json);
                        }
                        $win_output->refresh();
                    }
                    break;

                case 'agent_status':
                    $win_status->set_status($json);
                    if ($paused) $win_status->set_paused($paused);
                    $win_status->refresh();
                    break;

                case 'agent_hello':
                    $win_title->set_title(ncWindowTitle::TITLE_LEFT, $json->build_version);
                    if ($service == 0)
                        $win_title->set_title(ncWindowTitle::TITLE_RIGHT, $node);
                    else {
                        $win_title->set_title(ncWindowTitle::TITLE_RIGHT,
                            sprintf('%s:%d', $node, $service)
                        );
                    }
                    $win_title->refresh();
                    break;

                case 'definitions':
                    $applications = array();
                    foreach ($json->applications as $app) {
                        if (! property_exists($app, 'id') ||
                            ! property_exists($app, 'tag')) {
                            throw new Exception('Malformed JSON, applications.');
                        }

                        $applications[$app->id] = $app->tag;
                    }
                    $win_output->push_object(sprintf("Loaded %d application definitions.\n",
                        count($applications)
                    ));
                    $win_output->refresh();

                    $protocols = array();
                    foreach ($json->protocols as $protocol) {
                        if (! property_exists($protocol, 'id') ||
                            ! property_exists($protocol, 'tag')) {
                            throw new Exception('Malformed JSON, protocols.');
                        }

                        $protocols[$protocol->id] = $protocol->tag;
                    }
                    $win_output->push_object(sprintf("Loaded %d protocol definitions.\n",
                        count($protocols)
                    ));
                    $win_output->refresh();
                    break;
                }
            }
        } while ($rc > 0);
    }
}
catch (Exception $e) {
    $ex = sprintf("Exception: %s\n", $e->getMessage());

    $win_output->push_object($ex);
    $win_output->refresh();

    sleep(3);
    ncurses_end();

    echo $ex;

    exit($ex);
}

exit(0);

// vi: expandtab shiftwidth=4 softtabstop=4 tabstop=4
